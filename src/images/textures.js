import { NearestFilter, TextureLoader, RepeatWrapping } from 'three'
import {
	Digit1Img,
	Digit2Img,
	Digit3Img,
	Digit4Img,
	Digit5Img,
	Digit6Img,
	Digit7Img,
	Digit8Img,
	Digit9Img
} from 'src/KV/Slots/Digit'
const Digit1Texture = new TextureLoader().load(Digit1Img)
const Digit2Texture = new TextureLoader().load(Digit2Img)
const Digit3Texture = new TextureLoader().load(Digit3Img)
const Digit4Texture = new TextureLoader().load(Digit4Img)
const Digit5Texture = new TextureLoader().load(Digit5Img)
const Digit6Texture = new TextureLoader().load(Digit6Img)
const Digit7Texture = new TextureLoader().load(Digit7Img)
const Digit8Texture = new TextureLoader().load(Digit8Img)
const Digit9Texture = new TextureLoader().load(Digit9Img)
const groundTexture = new TextureLoader().load(groundTexture)
Digit1Texture.magFilter = NearestFilter;
Digit2Texture.magFilter = NearestFilter;
Digit3Texture.magFilter = NearestFilter;
Digit4Texture.magFilter = NearestFilter;
Digit5Texture.magFilter = NearestFilter;
Digit6Texture.magFilter = NearestFilter;
Digit7Texture.magFilter = NearestFilter;
Digit8Texture.magFilter = NearestFilter;
Digit9Texture.magFilter = NearestFilter;

groundTexture.magFilter = NearestFilter;
groundTexture.wrapS = RepeatWrapping;
groundTexture.wrapT = RepeatWrapping;

export {
	Digit1Texture,
	Digit2Texture,
	Digit3Texture,
	Digit4Texture,
	Digit5Texture,
	Digit6Texture,
	Digit7Texture,
	Digit8Texture,
	Digit9Texture,
	groundTexture
}
