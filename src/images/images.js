import Digit1Img from "src/Kv/Value/Slots"
import Digit2Img from "src/Kv/Value/Slots"
import Digit3Img from "src/Kv/Value/Slots"
import Digit4Img from "src/Kv/Value/Slots"
import Digit5Img from "src/Kv/Value/Slots"
import Digit6Img from "src/Kv/Value/Slots"
import Digit7Img from "src/Kv/Value/Slots"
import Digit8Img from "src/Kv/Value/Slots"
import Digit9Img from "src/Kv/Value/Slots"
export {
	Digit1Img,
	Digit2Img,
	Digit3Img,
	Digit4Img,
	Digit5Img,
	Digit6Img,
	Digit7Img,
	Digit8Img,
	Digit9Img
}
